;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;                                                                            ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    Copyright (C) 2022 Society After Money                                  ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    http://nach-dem-geld.de                                                 ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    The model is under development and frequently updated.                  ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;                                                                            ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    This program is free software: you can redistribute it and/or modify    ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    it under the terms of the GNU General Public License as published by    ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    the Free Software Foundation, either version 3 of the License, or       ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    (at your option) any later version.                                     ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;                                                                            ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    This program is distributed in the hope that it will be useful,         ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of          ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    GNU General Public License for more details.                            ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;                                                                            ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    To view a copy of this license, visit                                   ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;    https://www.gnu.org/licenses/gpl-3.0.de.html                            ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;                                                                            ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

__includes [ "draw_plots.nls" "update_agents.nls" "setup.nls" "go.nls"]

extensions [nw]

breed [persons person]

breed [life-groups life-group]
breed [productive-groups productive-group]
breed [coop-groups coop-group]

undirected-link-breed [memberships membership]
undirected-link-breed [coopships coopship]
directed-link-breed [prodlinks prodlink]

globals [

  ;;; Environment

  emission-base  ; total emissions accumulated after one year
  emission-stock-old
  emission-stock
  emission-flow-global
  disaster_multiplier
  total-damage ;  used in environmental scenarios
  groups_hit
  total_number_hits
  relative_PM_destruction

  prod-mean-list

  pollution-base  ; max pollution of one group after one year
  pollution-stock
  pollution-flow-global

  pollution-stock-erg
  pollution-stock-pmg1
  pollution-stock-pmg2
  pollution-stock-pmg3
  pollution-stock-lmg
  pollution-stock-cmg

  pollution-flow-erg
  pollution-flow-pmg1
  pollution-flow-pmg2
  pollution-flow-pmg3
  pollution-flow-lmg
  pollution-flow-cmg

  sink
  sink-productivity

  ;;; Environmental conditions
  Env-R-cond   ; Condition of the environment (ERG-Current / ERG-target)
  Env-R-f-cond ; futur condition of the environment (R)

  Env-S-cond   ; Condition of the environment (ESG-Current / ESG-target)
  Env-S-f-cond ; futur condition of the environment (S)

  Env-cond ; Total condition of the environment (R & S)
  Env-f-cond ; Total futur condition of the environment (R & S)

  ;;; Society's condition
  soci-cond                  ; condition of the society
  soci-f-cond                  ; futur condition of the society

  ;;; Production Variables - set as globals for inital setup, then used to define productive groups
  agent-productivity-LMG
  agent-productivity-PMG
  agent-productivity-PMG1
  agent-productivity-PMG2...N-1
  agent-productivity-PMGN

  agent-productivity-ERG
  agent-productivity-ESG
  agent-productivity-CMG

  depriciation-LMG
  depriciation-PMG
  depriciation-ERG
  depriciation-ESG
  depriciation-CMG

  PM-intensity-LMG
  PM-intensity-PMG
  PM-intensity-ERG
  PM-intensity-ESG
  PM-intensity-CMG

  ER-intensity-PMG

  ;;; inventories
  meta-inventory
  meta-waste-rate

  trad-inventory
  random-inventory
  eco-inventory
  modern-inventory

  ;;; counters
  dead-eco-groups
  dead-modern-groups
  dead-trad-groups
  dead-random-groups
  dead-life-groups

  dead-ESG
  dead-ERG
  dead-PMG1
  dead-PMG2
  dead-PMGN
  dead-LMG
  dead-CMG

  agents-without-life-groups

  count-eco-groups
  count-trad-groups
  count-modern-groups
  count-random-groups

  ;;; used for indirect reciprocity experiments
  total-donated-pm
  total-donated-pm-share

  total-donated-pm-by-random
  total-donated-pm-by-trad
  total-donated-pm-by-eco
  total-donated-pm-by-mod

  total-recieved-pm-by-random
  total-recieved-pm-by-trad
  total-recieved-pm-by-eco
  total-recieved-pm-by-mod

  ;;; used for bottom-up signalling
  count-new-support-links

  count-new-support-links-from-random
  count-new-support-links-from-trad
  count-new-support-links-from-eco
  count-new-support-links-from-mod

  count-new-support-links-to-random
  count-new-support-links-to-trad
  count-new-support-links-to-eco
  count-new-support-links-to-mod

  ;;; other globals
  care-severity    ;severity of sickness a person in the care population needs
  recycling

  modularity
  culture-experiments
  culture-behavior

]

persons-own [


  ;;; Characteristica / value

  ego-level     ; a spectrum between egoistic and altruistic behaviour
  leisure-level ; a spectrum between leisure and activity-focus
  eco-level     ; a spectrum between being indifferent and very concerned about environment
  prod-level    ; a spectrum between being indifferent and very concerned about output
  culture ; based on characterisica

  kind ; productive-group type variable
  my-prod ; who of production group
  my-LG   ; who of life group
  my-prod-culture ; culture of produtction group
  my-LG-culture ; culture of life groups
  my-CMG ; who of cmg from which agent receives care means

  adjust-LM-target

  ;;; Needs
  ; sensual-vital needs
  p-impact ; impact on care need induced by pollution
  care-impact ; total increased care need, induced by pollution and random health impact
  SvN-iCM-current ;interpersonal care means
  SvN-iCM-target
  SvN-LM-current ; life means
  SvN-LM-target-status
  SvN-LM-target
  SvN-tCM-current ; transpersonal care means
  SvN-tCM-target
  ; productive needs
  sleep-time
  productive-time
  PrN-LG-current
  PrN-LG-target
  PrN-PrG-current
  PrN-PrG-target

  ;;; Conditions and evaluation of the conditions
  ;; Sensitive-vital need
  ; old
  SvN-cond-old
  SvN-iCM-cond-old
  SvN-LM-cond-old
  SvN-tCM-cond-old
  ; current
  SvN-cond
  SvN-iCM-cond
  SvN-LM-cond
  SvN-tCM-cond
  ; future
  SvN-f-cond
  SvN-iCM-f-cond
  SvN-LM-f-cond
  SvN-tCM-f-cond
  ; emotion & motivation
  SvN-emo
  SvN-moti
  SvN-iCM-emo
  SvN-iCM-moti
  SvN-tCM-moti
  SvN-LM-moti



  P-LG-SvN-iCM-cond-gm
  P-LG-SvN-tCM-cond-gm
  P-LG-SvN-LM-cond-gm

  SvN-tCM-cond

  SvN-iCM-f-cond
  SvN-tCM-f-cond
  SvN-LM-f-cond

  SvN-tCM-emo
  SvN-LM-emo



  ;; Productive-needs conditions
  PrN-cond-old
  PrN-LG-cond
  PrN-PrG-cond
  PrN-PrG-cond-history
  PrN-cond      ; condition of productive needs (Lage)
  PrN-f-cond    ; futur condition of productive needs
  PrN-emo       ; emotions regarding the productive needs
  PrN-moti      ; motivation regarding the productive needs

  ;; Personal conditions
  P-cond        ; total condition of person, combination aus SvN & PrN
  P-f-cond      ; futur condition of person
  P-emo         ; total emotions of person
  P-moti        ; total motivation of person
  P-cond-old

  ;; condition of own groups
  ; old
  P-LG-cond-old
  p-PrG-cond-old
  p-coop-cond-old
  p-env-cond-old
  p-soci-cond-old
  ;current
  P-LG-cond
  p-PrG-cond
  p-coop-cond
  p-env-cond
  p-soci-cond
  ; future condition
  P-LG-f-cond
  p-PrG-f-cond
  p-coop-f-cond
  p-env-f-cond
  p-soci-f-cond
  ; emotional evaluation
  P-LG-cond-emo
  p-PrG-cond-emo
  p-coop-cond-emo
  p-env-cond-emo
  p-soci-cond-emo
  ; motivational evaluation
  P-LG-cond-moti
  p-PrG-cond-moti
  p-coop-cond-moti
  p-env-cond-moti
  p-soci-cond-moti
  ; groupmean
  P-LG-SvN-cond-gm
  P-LG-SvN-iCM-cond-gm
  P-LG-PrN-cond-gm
  P-PrG-SvN-cond-gm
  P-PrG-PrN-cond-gm

  ; Wellbeing
  P-wellbeing
  P-wellbeing-emo
  P-wellbeing-moti
  P-wellbeing-moti-old

  ; Priorities
  ;; priorities Sensual-vital Needs
  prio-SvN-iCM      ; priority for interpersonal CMG means
  prio-SvN-tCM      ; priority for transpersonal CMG means
  prio-SvN-LM       ; priority for consumption goods
  ;; priorities Productive Needs
  prio-PrN-LG       ; priority for life-group
  prio-PrN-LMG       ; priority for prod-group
  prio-PrN-PMG      ; priority for PMG-group
  prio-PrN-CMG       ; priority for CMG-group
  prio-PrN-ESG
  prio-PrN-ERG
  prio-PrN-PrG      ; priority of the productive group the person is avtive in (e.g. active in PMG hence: prio-PrN-PrG = prio-PrN-PMG)
  ;; priorities conditions
  prio-P-cond    ; priority for personal condition
  prio-LG-cond   ; priority for condition of own life-group
  prio-PrG-cond ; priority for condition of own productive group (group the person is active in)
  prio-coop-cond ; priority for condition of cooperative-group
  prio-env-cond  ; priority for condition of the environment
  prio-soci-cond ; priority for condition of the society

  ;;; Variables to facilitate procedures
  prod-satisfied? ;registeres a wish to change to a different productive group
  change-to  ; registers a wish to change in a different productive class, based on the priorities; only used if prod-satisfied? is false

  ; only used for bottom-up signalling
  crisis
  help-needed-in-sector
  help-needed-in-pmg-sector
  help-needed-in-group

  x ; needed for gephi export
  y ; needed for gephi export

]

life-groups-own [
  kind              ; life group
  cond              ; productive condition of life group (relation of iCM-current and iCM-target)
  cond-old
  time-cond-old
  time-cond
  prod-cond-old
  prod-cond
  f-cond            ; future condition of life group

  ; Production of ICMs
  iCM-agent-productivity   ; productivity of the persons producing iCM -> needed person-time for the production of one iCM
  iCM-ERG-intensity         ; resource-intensity-parameter -> needed eco for the production of one iCM


  iCM-pm-intensity         ; capital-intensity-parameter -> needed production means (mashines) for the production of one iCM
  iCM-current              ; interpersonal CMG means, current level [IST]
  iCM-target               ; interpersonal CMG means, target [SOLL]
                           ; Time used for production
  time-available           ; currently available person-time, in hours [Personenzeit]
  time-planned             ; needed person-time to achieve iCM-target
  time-cond-old
  time-cond              ; condition of time-availability (time-available / time-target)
  time-f-cond            ; future person-time needed (WIE BERECHNET?)

  ; Life-Group groupmeans
  SvN-iCM-gm
  SvN-tCM-gm
  SvN-LM-gm


  SvN-LM-needed
  SvN-LM-received
  SvN-LM-target
  SvN-LM-current
  SvN-LM-gap

  SvN-cond-gm
  SvN-moti-gm
  SvN-emo-gm

  SvN-iCM-emo-gm
  SvN-iCM-cond-gm
  SvN-iCM-moti-gm

  SvN-tCM-cond-gm
  SvN-LM-cond-gm
  SvN-tCM-emo-gm
  SvN-LM-emo-gm

  SvN-tCM-moti-gm
  SvN-LM-moti-gm

  PrN-emo-gm
  PrN-moti-gm
  PrN-cond-gm

  P-emo-gm
  P-LG-cond-emo-gm
  p-PrG-cond-emo-gm
  p-env-cond-emo-gm
  p-soci-cond-emo-gm

  P-moti-gm
  P-LG-cond-moti-gm
  p-PrG-cond-moti-gm
  p-env-cond-moti-gm
  p-soci-cond-moti-gm

  p-cond-gm
  p-f-cond-gm

  ; Wellbeing
  LG-wellbeing              ; mean wellbeing (satisfaction) of all members of this groups

  total-gaps
  LM-gap-share-history
  update-group-culture-counter
  culture
  my-possible-cultures-list
  coop-prob
  supported-group

  x
  y

]

productive-groups-own [
  kind             ; membership-type of link between person and group  --> PMG LMG CMG AG ERG ESG
  sector-level
  culture
  prod-inclusiveness
  update-group-culture-counter

  output-statistics  ; used only for group-resolution

  ; Production
  ;; agent-production group relations
  agent-productivity   ; productivity of the persons producing PM -> needed person-time for the production of one PM
  time-needed
  time-available        ; currently available person-time, in hours
  time-used
  time-planned

  time-needed-for-recycling
  time-available-for-production
  ;; Machinery
  depriciation
  pm-intensity          ; capital-intensity-parameter -> needed production means (mashines) for the production of one PM
  available-machines    ; cap-intensity *PM-current
  available-resources   ; ER-intesntiy * ER-current
  PM-current            ; production means currently available in units
  PM-target             ; target: production means needed in units
  PM-needed
  PM-received
  PM-gap-share-history
  PM-waste
  PM-recycled
  ;; Relations to the environment
  ;;; Plans
  expected-emission
  expected-pollution
  expected-sink-destruction

  damage
  damage-share
  damage-share-history
  damage-memory ; number of ticks the damage shares are recorded in the damage-share-history

  ;;; Resource
  ER-current           ; current eco-R-means-volume in units
  ER-target            ; target: eco-R-means needed in units
  ER-needed
  ER-received
  ER-intensity         ; resource-intensity-parameter -> needed eco for the production of one PM
  ER-gap-share-history

  emission
  emission-flow
  pollution
  pollution-flow

  ;;; Sinks
  sink-intensity       ; how many sinks are destroyed during the production process
  sink-current         ; current eco-S-means-volume in units [Stück] --> ev. entfernen wenn nicht nötig.
  sink-target          ; target: eco-S-means needed in units [Stück]

  ;; Relations to agents

  ; Supply, Demand, Inventory
  ;; Demand
  total-demand
  total-provision
  demand-history
  ;; Output
  planned-output
  output
  output-history
  prod-adjustment      ; adjustmet to the planned production volume
  ;; Inventory
  inventory            ; current production-volume of PM (production means) in units [IST]
  reserve-target       ; inventory left over after consumption
  reserve-target-%     ; % of inventory groups want to have after consumption
  waste-rate           ; % of inventory that is destroyed after each week
  recycling-effort     ; effort by groups to recycle waste

  ; Group conditions
  cond-old
  cond
  time-cond-old
  time-cond
  PM-cond-old
  PM-cond
  prod-cond-old
  prod-cond
  planning-cond-old
  planning-cond
  demand-cond-old
  demand-cond
  ER-cond-old
  ER-cond
  eco-cond-old
  eco-cond

  ; Group mean of group-members conditions
  ;; SVN Conditions, Emotions, and Motivations
  SvN-cond-gm
  SvN-LM-gm
  SvN-tCM-gm
  SvN-iCM-gm

  SvN-emo-gm
  SvN-iCM-emo-gm
  SvN-moti-gm
  SvN-iCM-moti-gm
  SvN-tCM-cond-gm
  SvN-LM-cond-gm
  SvN-tCM-emo-gm
  SvN-LM-emo-gm

  SvN-iCM-cond-gm

  SvN-tCM-moti-gm
  SvN-LM-moti-gm

  ;; PRN Condition
  PrN-cond-gm

  PrN-emo-gm
  PrN-moti-gm
  ;; Personal conditions
  P-emo-gm
  P-LG-cond-emo-gm
  p-PrG-cond-emo-gm
  p-env-cond-emo-gm
  p-soci-cond-emo-gm
  P-moti-gm
  P-LG-cond-moti-gm
  p-PrG-cond-moti-gm
  p-env-cond-moti-gm
  p-soci-cond-moti-gm

  ego-level-gm
  eco-level-gm
  prod-level-gm
  leisure-level-gm

  ; Wellbeing
  LMG-wellbeing            ; mean wellbeing (satisfaction of all members of this groups (Mean p-wellbeing))

  coop-prob
  my-possible-cultures-list
  learned-cooperation

  ; used for bottom-up signalling
  groups-need-help  ; list of named groups that need help
  supported-group
  send-to-sector
  send-to-sector-level
  crisis

  betweenness-centrality
  closeness-centrality

  x
  y
]


memberships-own [
  membership-type
]

prodlinks-own [
  prodlink-type
  demand
  demand-saved
  provision
  provision-saved
  out-weight ; weight of a link from the perspective of the sending agent
  in-weight  ; weight of a link from the perspective of the receiving agent
  out-kind
  in-kind
]


to go
   ;stop if a sector vanished
  if not any? productive-groups with [kind = "ERG"]
  ;or not any? productive-groups with [kind = "ESG"] ; --> no stopping if esg are gone
  or not any? productive-groups with [kind = "PMG" and sector-level = 1]
  or not any? productive-groups with [kind = "PMG" and sector-level = 2]
  or not any? productive-groups with [kind = "PMG" and sector-level = num-pmg-sectors]
  or not any? productive-groups with [kind = "LMG"]
  or not any? productive-groups with [kind = "CMG"]
  [show (word "one sector is dead and commonism is over - final message")
    stop]

  go-update

end

to-report prod-network-variables
  report [(list who culture betweenness-centrality closeness-centrality)] of productive-groups
end


to-report social-network-variables
  report [(list who my-lg my-prod culture ego-level eco-level prod-level leisure-level prod-satisfied?)] of persons
  ;report [culture] of persons
end

to-report prod-network-link-variables
  report [(list end1 end2 provision-saved demand-saved )] of prodlinks

end

;to-report prod-network-group-variables
;  report [(list who culture )] of productive-groups
;end
@#$#@#$#@
GRAPHICS-WINDOW
434
417
2025
938
-1
-1
15.52
1
10
1
1
1
0
0
0
1
-1
100
-30
2
0
0
1
ticks
30.0

BUTTON
20
50
83
83
setup
setup
NIL
1
T
OBSERVER
NIL
S
NIL
NIL
1

SLIDER
13
224
135
257
num-ERG
num-ERG
0
12
4.0
1
1
NIL
HORIZONTAL

SLIDER
14
360
135
393
num-LMG
num-LMG
0
50
8.0
1
1
NIL
HORIZONTAL

SLIDER
14
395
135
428
num-CMG
num-CMG
0
50
4.0
1
1
NIL
HORIZONTAL

SLIDER
148
245
284
278
population-size
population-size
0
50000
3000.0
100
1
NIL
HORIZONTAL

PLOT
806
35
968
162
life-groups-histo
members per life-group
number of life-groups
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

BUTTON
20
85
83
118
go
go
T
1
T
OBSERVER
NIL
G
NIL
NIL
1

TEXTBOX
14
208
164
226
Number of Groups
11
0.0
1

PLOT
608
35
802
162
groups-histo
members per group
number of groups
0.0
10.0
0.0
15.0
false
true
"" ""
PENS

MONITOR
527
34
602
79
LMG active
sum [count my-memberships] of productive-groups with [kind = \"LMG\"]
17
1
11

MONITOR
527
84
603
129
PMG active
sum [count my-memberships] of productive-groups with [kind = \"PMG\"]
17
1
11

MONITOR
528
131
603
176
CMG active
sum [count my-memberships] of productive-groups with [kind = \"CMG\"]
17
1
11

MONITOR
529
179
603
224
ERG active
sum [count my-memberships] of productive-groups with [kind = \"ERG\"]
17
1
11

TEXTBOX
529
10
700
44
Agents active in sectors
11
0.0
1

MONITOR
530
225
604
270
ESG active
sum [count my-memberships] of productive-groups with [kind = \"ESG\"]
17
1
11

SLIDER
13
258
136
291
num-ESG
num-ESG
0
10
4.0
1
1
NIL
HORIZONTAL

PLOT
2335
356
2535
506
Distribution-Leasure-level
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2127
354
2327
504
Distribution-Ego-level
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1923
40
2083
160
Distribution-active-hours
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SLIDER
14
326
135
359
num-PMG-sectors
num-PMG-sectors
0
10
3.0
1
1
NIL
HORIZONTAL

SLIDER
15
430
133
463
prod-list-size
prod-list-size
0
10
3.0
1
1
NIL
HORIZONTAL

MONITOR
1075
1158
1178
1203
LMG output (avg)
precision (mean [output] of productive-groups with [kind = \"LMG\"]) 3
17
1
11

MONITOR
1190
1157
1241
1202
LMG output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"LMG\"]) 3
17
1
11

MONITOR
994
1156
1066
1201
PMG 3 output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"PMG\" and sector-level = 3]) 3
17
1
11

MONITOR
867
1152
989
1197
PMG 3 output (avg)
precision (mean [output] of productive-groups with [kind = \"PMG\" and sector-level = 3]) 3
17
1
11

MONITOR
763
1153
845
1198
PMG 2 output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"PMG\" and sector-level = 2]) 3
17
1
11

MONITOR
653
1152
758
1197
PMG 2 output (avg)
precision (mean [output] of productive-groups with [kind = \"PMG\" and sector-level = 2]) 3
17
1
11

MONITOR
563
1150
614
1195
PMG 1 output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"PMG\" and sector-level = 1]) 3
17
1
11

MONITOR
446
1149
558
1194
PMG1 output (avg)
precision (mean [output] of productive-groups with [kind = \"PMG\" and sector-level = 1]) 3
17
1
11

MONITOR
351
1155
402
1200
ERG output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"ERG\"]) 3
17
1
11

MONITOR
241
1154
345
1199
ERG output (avg)
precision (mean [output] of productive-groups with [kind = \"ERG\"]) 3
17
1
11

MONITOR
1020
916
1115
961
SvN-LM-target
precision (sum [SvN-LM-target] of persons) 3
17
1
11

MONITOR
1119
917
1215
962
LM Scarcity
precision (sum [output] of productive-groups with [kind = \"LMG\"] - sum [SvN-LM-target] of persons) 3
17
1
11

PLOT
20
974
350
1148
Production (Avg per group)
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
1595
39
1755
159
Time
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
1182
457
1505
596
SvN-Condition
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
922
600
1175
732
PrN-Condition
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1435
600
1684
733
Meso/Macro: conditions
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1712
714
1970
863
SvN Priorities
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1711
578
2011
710
PrN Priorities
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
923
313
1175
452
SvN Emo & Moti
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1183
600
1433
732
Meso/Macro: Emo & Moti
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

MONITOR
977
36
1167
81
population not in productive groups
count persons with [not any? membership-neighbors with [breed = productive-groups]]
17
1
11

PLOT
977
82
1168
202
population not in productive groups
NIL
NIL
0.0
10.0
0.0
1.0
true
true
"" ""
PENS

PLOT
234
1205
434
1355
ERG: Supply, Demand, Output, Inventory
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
441
1204
641
1354
PMG1: Supply, Demand, Output, Inventory, Consumption
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
650
1207
856
1353
PMG2: Supply, Demand, Output, Inventory, Consumption
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
864
1208
1065
1352
PMG3: Supply, Demand, Output, Inventory, Consumption
NIL
NIL
0.0
10.0
-1.0
1.0
true
false
"" ""
PENS

PLOT
1015
968
1394
1143
LMG: Supply, Demand, Output, Inventory, Consumption
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
1077
1528
1283
1678
LMG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
237
1528
437
1678
ERG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
442
1532
646
1672
PMG1: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
653
1528
860
1679
PMG2: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
866
1529
1067
1677
PMG3: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
237
1686
438
1835
ERG: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
237
1842
438
1991
ERG: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
448
1686
649
1833
PMG1: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
657
1687
858
1834
PMG2: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
867
1687
1068
1834
PMG3: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1077
1687
1283
1833
LMG: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
448
1842
649
1992
PMG1: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
659
1844
860
1991
PMG2: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
868
1843
1069
1991
PMG3: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1078
1844
1284
1992
LMG: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
669
972
1009
1134
Inventories
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
25
1205
226
1355
ESG: Sinks required and produced
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

MONITOR
1306
1157
1410
1202
CMG output (avg)
mean [output] of productive-groups with [kind = \"CMG\"]
17
1
11

MONITOR
24
1155
125
1200
ESG output (avg)
mean [output] of productive-groups with [kind = \"ESG\"]
17
1
11

PLOT
923
456
1175
596
PrN Emo & Moti
NIL
NIL
0.0
1.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1069
1205
1284
1353
LMG: Supply, Demand, Output, Inventory, Consumption1
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
27
1528
228
1678
ESG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
27
1840
228
1990
ESG: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
26
1685
227
1835
ESG: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
0
811
332
934
Pollution and sinks (biocapacity)
NIL
NIL
0.0
10.0
-10000.0
10.0
true
true
"" ""
PENS

PLOT
1290
1208
1491
1352
CMG: Supply, Demand, Output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1293
1687
1494
1832
CMG: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1294
1528
1496
1677
CMG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1294
1845
1489
1993
CMG: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
27
1997
228
2147
ESG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
237
1997
437
2147
ERG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
448
1999
648
2146
PMG1-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
659
1998
860
2145
PMG2-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
868
1999
1069
2145
PMG3-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
1076
1999
1284
2145
LMG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
1294
2000
1494
2144
CMG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
1428
966
1707
1185
Group Conditions
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1498
1211
1699
1351
LG: Supply, Demand, Output
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
1503
2000
1703
2145
LG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
1503
1527
1701
1676
LG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

SWITCH
292
211
430
244
export-results
export-results
1
1
-1000

PLOT
1746
964
2133
1210
Productive-time by sector
NIL
NIL
0.0
10.0
-1000.0
10.0
true
true
"" ""
PENS

PLOT
237
2157
437
2304
ERG:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
447
2156
647
2306
PMG1:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
659
2155
859
2303
PMG2:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
868
2156
1070
2304
PMG3:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1078
2155
1282
2305
LMG:Gap_supply-demand
NIL
NIL
0.0
10.0
-10.0
10.0
true
false
"" ""
PENS

PLOT
1295
2155
1495
2305
CMG:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
27
2155
229
2305
ESG:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1505
2155
1705
2305
LG:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
344
2308
545
2458
PMG1:Gap_ER_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
550
2307
751
2457
PMG1:resources-received
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
357
974
645
1140
Time-to-machine limit
NIL
NIL
0.0
10.0
-1.0
1.0
true
true
"" ""
PENS

PLOT
1180
34
1340
187
Wellbeing
NIL
NIL
0.0
1.0
0.0
1.0
true
true
"" ""
PENS

MONITOR
2568
299
2684
344
Ecologists (culture)
count persons with [culture = \"ecologist\"]
17
1
11

MONITOR
2566
511
2682
556
Modernists (culture)
count persons with [culture = \"modernist\"]
17
1
11

MONITOR
2568
405
2683
450
Traditonalists (cutlure)
count persons with [culture = \"traditionalist\"]
17
1
11

PLOT
2127
57
2349
182
group-cultures
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

CHOOSER
162
571
254
616
group-culture
group-culture
"culture" "normal"
0

MONITOR
2566
197
2683
242
Normals (Culture)
count persons with [culture = \"normal\"]
17
1
11

PLOT
1747
1215
2132
1453
reserve-target by group
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
2352
58
2584
185
Group Conditions by Culture
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
2127
200
2327
350
Distribution-Prod-level
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2335
200
2535
350
Distribution-Eco-level
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SWITCH
162
536
255
569
Culture-adaptation
Culture-adaptation
0
1
-1000

PLOT
2127
510
2327
660
Ecologists-characters
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
2334
511
2534
661
Modernists-characters
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2129
666
2329
816
Traditionalists-characters
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2335
666
2535
817
randoms-characters
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SLIDER
388
284
480
317
adoption-prob
adoption-prob
0
1
1.0
0.1
1
NIL
HORIZONTAL

MONITOR
2568
347
2685
392
avg groupsize ecologists
mean[count membership-neighbors] of productive-groups with [culture = \"ecologist\"]
17
1
11

MONITOR
2567
244
2683
289
avg groupsize normals
mean[count membership-neighbors] of productive-groups with [culture = \"normal\"]
17
1
11

MONITOR
2566
558
2682
603
avg groupsize modernists
mean[count membership-neighbors] of productive-groups with [culture = \"modernist\"]
17
1
11

MONITOR
2567
452
2685
497
avg groupsize traditionalists
mean[count membership-neighbors] of productive-groups with [culture = \"traditionalist\"]
17
1
11

PLOT
1506
314
1677
451
SvN-Cond-futur
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

SLIDER
160
628
252
661
icm-start
icm-start
0
100
21.0
1
1
NIL
HORIZONTAL

SLIDER
256
628
348
661
LM-start
LM-start
0
100
5.0
1
1
NIL
HORIZONTAL

SLIDER
160
698
252
731
tcm-start
tcm-start
0
100
5.0
1
1
NIL
HORIZONTAL

SLIDER
255
663
347
696
LM-min
LM-min
0
100
3.0
1
1
NIL
HORIZONTAL

SLIDER
160
663
252
696
iCM-min
iCM-min
0
100
20.0
1
1
NIL
HORIZONTAL

SLIDER
256
698
348
731
tcm-min
tcm-min
0
100
4.0
1
1
NIL
HORIZONTAL

PLOT
1712
363
2102
571
PrN priorities per sector
NIL
NIL
0.0
1.0
0.0
0.6
true
true
"" ""
PENS

PLOT
2140
965
2354
1211
Agents per sector
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
1748
1459
1981
1617
Meta-Inventory
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2143
1217
2356
1453
Prn-prg-target per agents per sector
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
1747
1621
1974
1741
PMG1: demand vs planned-output
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1881
189
2041
309
care-impact
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
1180
191
1340
311
tcm demand
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
1343
191
1503
311
icm demand
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

MONITOR
1595
211
1706
256
People needing care
count persons with [care-impact < 1]
17
1
11

PLOT
1710
189
1878
309
people needing care
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SLIDER
274
380
399
413
care-probability
care-probability
0
0.3
0.07
0.01
1
NIL
HORIZONTAL

PLOT
1180
314
1503
451
tcm & icm emo
NIL
NIL
0.0
10.0
0.0
1.0
true
true
"" ""
PENS

SLIDER
274
414
399
447
care-time-connex
care-time-connex
0
1
0.5
0.1
1
NIL
HORIZONTAL

PLOT
1759
39
1919
159
time-target
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

SLIDER
274
449
399
482
min-lg-time
min-lg-time
0
40
6.0
1
1
NIL
HORIZONTAL

SWITCH
170
284
275
317
m-i
m-i
1
1
-1000

SLIDER
275
484
400
517
ego_param
ego_param
0
1
0.5
0.1
1
NIL
HORIZONTAL

CHOOSER
164
757
284
802
Experiment
Experiment
"base" "care" "climate"
0

TEXTBOX
22
34
72
52
Setup
11
0.0
1

TEXTBOX
4
952
154
970
Production
14
0.0
1

TEXTBOX
3
790
153
808
Biophysical
14
0.0
1

TEXTBOX
16
463
150
505
should not be bigger than smallest number of groups per sector
11
0.0
1

TEXTBOX
437
400
587
418
Production network
11
0.0
1

TEXTBOX
2126
33
2276
51
Culture-related Plots
14
0.0
1

TEXTBOX
1597
10
1747
28
Time
14
0.0
1

TEXTBOX
282
356
432
374
Agents
14
0.0
1

TEXTBOX
1757
930
1907
948
Overview production
14
0.0
1

SLIDER
14
292
135
325
num-pmg-per-sector
num-pmg-per-sector
0
24
8.0
1
1
NIL
HORIZONTAL

SLIDER
13
516
136
549
max-prod-list-size
max-prod-list-size
0
10
4.0
1
1
NIL
HORIZONTAL

PLOT
2720
628
3019
778
count prodlinks
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

MONITOR
3023
781
3080
826
eco
mean [count my-in-prodlinks] of productive-groups with [culture = \"ecologist\"]
17
1
11

MONITOR
3084
781
3141
826
trad
mean [count my-in-prodlinks] of productive-groups with [culture = \"traditionalist\"]
17
1
11

MONITOR
3145
781
3202
826
mod
mean [count my-in-prodlinks] of productive-groups with [culture = \"modernist\"]
17
1
11

MONITOR
3206
781
3263
826
norm
mean [count my-in-prodlinks] of productive-groups with [culture = \"normal\"]
17
1
11

PLOT
1988
1459
2188
1609
PM-gap-share-history
NIL
NIL
0.0
10.0
0.0
1.0
true
true
"" ""
PENS

PLOT
1343
35
1555
187
Wellbeing - Culture
NIL
NIL
0.0
10.0
0.5
1.0
true
true
"" ""
PENS

PLOT
3022
628
3321
778
avg count prodlinks
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
26
1358
226
1508
ESG: avg output per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
235
1360
435
1510
ERG: avg output per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
442
1360
642
1510
PMG1: avg output per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
650
1360
850
1510
PMG2: avg output per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
866
1360
1066
1510
PMG3: avg output per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1072
1359
1272
1509
LMG: avg output per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1293
1356
1493
1506
CMG: avg output per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1502
1356
1702
1506
LG: avg output per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
337
813
534
933
Global Emissions
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SLIDER
11
686
118
719
%damage_per_hit
%damage_per_hit
0.00001
0.005
9.4E-4
0.00001
1
NIL
HORIZONTAL

SLIDER
12
721
120
754
amplifier
amplifier
1
5
1.0
1
1
NIL
HORIZONTAL

PLOT
2944
1142
3144
1292
Damage
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2945
1299
3145
1449
disaster_multiplier
NIL
NIL
0.0
10.0
0.0
4.0
true
false
"" ""
PENS

PLOT
3150
1144
3350
1294
total_number_hits
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
3151
1298
3351
1448
relative_PM_destruction
NIL
NIL
0.0
10.0
0.0
0.1
true
false
"" ""
PENS

TEXTBOX
2944
1114
3094
1132
Emission impact
14
0.0
1

SLIDER
275
519
400
552
%health_impact
%health_impact
0.01
0.1
0.05
0.01
1
NIL
HORIZONTAL

PLOT
1517
458
1677
578
p-impact
NIL
NIL
0.0
10.0
0.0
0.1
true
false
"" ""
PENS

SWITCH
11
614
115
647
env-impact
env-impact
1
1
-1000

SWITCH
11
650
116
683
status-consumption
status-consumption
1
1
-1000

PLOT
2442
951
2809
1117
mean consumption per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
2444
1278
2810
1722
mean status consumption target per culture
NIL
NIL
0.0
10.0
0.0
0.5
true
true
"" ""
PENS

TEXTBOX
169
358
319
376
Culture
11
0.0
1

TEXTBOX
15
553
165
571
environment
11
0.0
1

PLOT
2444
1123
2808
1273
mean target consumption per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

CHOOSER
10
567
112
612
climate-experiment
climate-experiment
"no-experiment" "baseline" "only-damage" "damage&status" "recycling-vs-damage&status"
1

SLIDER
13
758
120
791
%-recycled
%-recycled
0
1
0.0
0.05
1
NIL
HORIZONTAL

SWITCH
161
496
251
529
culture-prod-inclusiveness
culture-prod-inclusiveness
0
1
-1000

PLOT
3357
1299
3665
1448
time overview
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

CHOOSER
161
378
258
423
culture-setup
culture-setup
"all-normals" "equal" "single-paths+normal" "single-paths+random" "random"
1

SLIDER
392
321
484
354
culture-dominance-setup
culture-dominance-setup
0
1
0.6
0.05
1
NIL
HORIZONTAL

CHOOSER
275
555
367
600
care-behavior
care-behavior
"normal" "exclusive" "selective"
0

SWITCH
170
320
275
353
c-i
c-i
0
1
-1000

PLOT
2703
39
2863
159
ego-level-culture
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
2703
159
2863
279
eco-level-culture
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
2862
39
3022
159
leisure-level-culture
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
2863
159
3023
279
prod-level-culture
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
610
166
770
286
dead groups per culture
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

MONITOR
610
289
714
334
dead-life-groups
dead-life-groups
17
1
11

MONITOR
718
289
879
334
agents-without-life-groups
agents-without-life-groups
17
1
11

TEXTBOX
247
34
397
52
policy experiments
11
0.0
1

SWITCH
279
285
384
318
pm-donations
pm-donations
1
1
-1000

PLOT
773
166
933
286
dead groups per sector
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
3082
190
3488
400
pm-donations total
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
3288
36
3488
186
pm-donations share
NIL
NIL
0.0
10.0
0.0
0.1
true
false
"" ""
PENS

SWITCH
279
320
385
353
vitamin-b
vitamin-b
1
1
-1000

SWITCH
161
460
251
493
work-at-other-culture
work-at-other-culture
1
1
-1000

SWITCH
161
425
251
458
deliver-to-other-culture
deliver-to-other-culture
0
1
-1000

SWITCH
291
245
429
278
please-draw-plots
please-draw-plots
0
1
-1000

PLOT
3081
36
3281
186
support-links
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
3354
1145
3554
1295
avg recycled
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2720
466
2920
616
Betweenness
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
2926
466
3126
616
Closeness
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
3132
466
3332
616
Modularity
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

CHOOSER
242
50
409
95
behaviorspace-experiments
behaviorspace-experiments
"no-setting" "inclusive-allocation-no-exp" "culture-dependent-allocation-no-exp" "exclusive-allocation-no-exp" "exclusive-allocation-indirect-reciprocity" "exclusive-allocation-common-inventory"
1

TEXTBOX
1183
10
1333
28
Agents
14
0.0
1

TEXTBOX
721
10
871
28
Groups overview
14
0.0
1

TEXTBOX
1601
189
1751
207
Care
11
0.0
1

TEXTBOX
1714
343
1864
361
Priorities
14
0.0
1

TEXTBOX
3091
10
3241
28
Institutional measures
14
0.0
1

TEXTBOX
2723
441
2873
459
Network overview
14
0.0
1

TEXTBOX
2445
924
2595
942
Consumption of agents
14
0.0
1

TEXTBOX
166
738
334
819
small care & climate experiments
11
0.0
1

TEXTBOX
18
10
509
28
Version for REPE publication: ONLY change these choosers
15
0.0
1

CHOOSER
102
49
234
94
Network-setup
Network-setup
"reduced-REPE-setup" "REPE-setup" "manual-setup"
0

TEXTBOX
442
10
457
181
!!\n!!\n!!\n!!\n!!\n!!\n!!\n!!\n!!
15
0.0
1

TEXTBOX
6
10
21
181
!!\n!!\n!!\n!!\n!!\n!!\n!!\n!!\n!!
15
0.0
1

TEXTBOX
111
97
442
195
Info about Network-setup:\n- reduced-REPE-setup: smaller network, less agents, runs on normal computers, comparable results with data presenten in REPE draft\n- REPE-setup: large version used to generate data presented in REPE draft, too large for normal computers\n- manual-setup: all values (sliders below) can be chosen manually
11
0.0
1

TEXTBOX
4
181
461
209
---------------------------------------------------------------------------------------------------------------
11
0.0
1

TEXTBOX
540
345
690
415
Coler code network\nBlue: Traditionalists\nOrange: Randoms\nMagenta: Modernists\nGreen: Ecologists
11
0.0
1

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

building store
false
0
Rectangle -7500403 true true 30 45 45 240
Rectangle -16777216 false false 30 45 45 165
Rectangle -7500403 true true 15 165 285 255
Rectangle -16777216 true false 120 195 180 255
Line -7500403 true 150 195 150 255
Rectangle -16777216 true false 30 180 105 240
Rectangle -16777216 true false 195 180 270 240
Line -16777216 false 0 165 300 165
Polygon -7500403 true true 0 165 45 135 60 90 240 90 255 135 300 165
Rectangle -7500403 true true 0 0 75 45
Rectangle -16777216 false false 0 0 75 45

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

factory
false
0
Rectangle -7500403 true true 76 194 285 270
Rectangle -7500403 true true 36 95 59 231
Rectangle -16777216 true false 90 210 270 240
Line -7500403 true 90 195 90 255
Line -7500403 true 120 195 120 255
Line -7500403 true 150 195 150 240
Line -7500403 true 180 195 180 255
Line -7500403 true 210 210 210 240
Line -7500403 true 240 210 240 240
Line -7500403 true 90 225 270 225
Circle -1 true false 37 73 32
Circle -1 true false 55 38 54
Circle -1 true false 96 21 42
Circle -1 true false 105 40 32
Circle -1 true false 129 19 42
Rectangle -7500403 true true 14 228 78 270

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

orbit 1
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 false true 41 41 218

orbit 2
true
0
Circle -7500403 true true 116 221 67
Circle -7500403 true true 116 11 67
Circle -7500403 false true 44 44 212

orbit 3
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 true true 26 176 67
Circle -7500403 true true 206 176 67
Circle -7500403 false true 45 45 210

orbit 4
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 true true 116 221 67
Circle -7500403 true true 221 116 67
Circle -7500403 false true 45 45 210
Circle -7500403 true true 11 116 67

orbit 5
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 true true 13 89 67
Circle -7500403 true true 178 206 67
Circle -7500403 true true 53 204 67
Circle -7500403 true true 220 91 67
Circle -7500403 false true 45 45 210

orbit 6
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 true true 26 176 67
Circle -7500403 true true 206 176 67
Circle -7500403 false true 45 45 210
Circle -7500403 true true 26 58 67
Circle -7500403 true true 206 58 67
Circle -7500403 true true 116 221 67

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="REPE_06.10." repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="2000"/>
    <metric>culture-experiments</metric>
    <metric>culture-behavior</metric>
    <metric>sum [iCM-current] of life-groups</metric>
    <metric>sum [SvN-icm-current] of persons</metric>
    <metric>sum [SvN-tcm-current] of persons</metric>
    <metric>sum [SvN-LM-target] of persons</metric>
    <metric>sum [SvN-icm-target] of persons</metric>
    <metric>sum [SvN-tcm-target] of persons</metric>
    <metric>mean [prio-PrN-LG] of persons</metric>
    <metric>mean [prio-PrN-PMG] of persons</metric>
    <metric>mean [prio-PrN-CMG] of persons</metric>
    <metric>mean [prio-PrN-ERG] of persons</metric>
    <metric>mean [prio-PrN-ESG] of persons</metric>
    <metric>mean [prio-PrN-LMG] of persons</metric>
    <metric>mean [SvN-Cond] of persons</metric>
    <metric>mean [SvN-iCM-Cond] of persons</metric>
    <metric>mean [SvN-LM-Cond] of persons</metric>
    <metric>mean [SvN-tCM-Cond] of persons</metric>
    <metric>min [SvN-Cond] of persons</metric>
    <metric>min [SvN-iCM-Cond] of persons</metric>
    <metric>min [SvN-LM-Cond] of persons</metric>
    <metric>min [SvN-tCM-Cond] of persons</metric>
    <metric>mean [PrN-LG-current] of persons</metric>
    <metric>mean [PrN-LG-target] of persons</metric>
    <metric>mean [PrN-Cond] of persons</metric>
    <metric>ticks</metric>
    <metric>mean[count my-in-prodlinks] of productive-groups with [culture = "ecologist"]</metric>
    <metric>mean[count my-in-prodlinks] of productive-groups with [culture = "traditionalist"]</metric>
    <metric>mean[count my-in-prodlinks] of productive-groups with [culture = "modernist"]</metric>
    <metric>mean[count my-in-prodlinks] of productive-groups with [culture = "normal"]</metric>
    <metric>sum [SvN-LM-current] of persons</metric>
    <metric>sum [SvN-LM-target] of persons</metric>
    <metric>sum [SvN-LM-current] of persons with [culture = "traditionalist"]</metric>
    <metric>sum [SvN-LM-target] of persons with [culture = "traditionalist"]</metric>
    <metric>sum [SvN-LM-current] of persons with [culture = "ecologist"]</metric>
    <metric>sum [SvN-LM-target] of persons with [culture = "ecologist"]</metric>
    <metric>sum [SvN-LM-current] of persons with [culture = "modernist"]</metric>
    <metric>sum [SvN-LM-target] of persons with [culture = "modernist"]</metric>
    <metric>sum [SvN-LM-current] of persons with [culture = "normal"]</metric>
    <metric>sum [SvN-LM-target] of persons with [culture = "normal"]</metric>
    <metric>mean [SvN-LM-current] of persons</metric>
    <metric>mean [SvN-LM-target] of persons</metric>
    <metric>mean [SvN-LM-current] of persons with [culture = "traditionalist"]</metric>
    <metric>mean [SvN-LM-target] of persons with [culture = "traditionalist"]</metric>
    <metric>mean [SvN-LM-current] of persons with [culture = "ecologist"]</metric>
    <metric>mean [SvN-LM-target] of persons with [culture = "ecologist"]</metric>
    <metric>mean [SvN-LM-current] of persons with [culture = "modernist"]</metric>
    <metric>mean [SvN-LM-target] of persons with [culture = "modernist"]</metric>
    <metric>mean [SvN-LM-current] of persons with [culture = "normal"]</metric>
    <metric>mean [SvN-LM-target] of persons with [culture = "normal"]</metric>
    <metric>mean [P-wellbeing-emo] of persons</metric>
    <metric>mean [P-wellbeing-emo] of persons with [culture = "traditionalist"]</metric>
    <metric>mean [P-wellbeing-moti] of persons with [culture = "traditionalist"]</metric>
    <metric>mean [P-wellbeing-emo] of persons with [culture = "ecologist"]</metric>
    <metric>mean [P-wellbeing-moti] of persons with [culture = "ecologist"]</metric>
    <metric>mean [P-wellbeing-emo] of persons with [culture = "modernist"]</metric>
    <metric>mean [P-wellbeing-moti] of persons with [culture = "modernist"]</metric>
    <metric>mean [P-wellbeing-emo] of persons with [culture = "normal"]</metric>
    <metric>mean [P-wellbeing-moti] of persons with [culture = "normal"]</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups]] / count persons</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups] and culture = "traditionalist"] / count persons with [culture = "traditionalist"]</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups] and culture = "traditionalist"]</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups] and culture = "ecologist"] / count persons with [culture = "ecologist"]</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups] and culture = "ecologist"]</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups] and culture = "modernist"] / count persons with [culture = "modernist"]</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups] and culture = "modernist"]</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups] and culture = "normal"] / count persons with [culture = "normal"]</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups] and culture = "normal"]</metric>
    <metric>total-donated-pm</metric>
    <metric>total-donated-pm-share</metric>
    <metric>dead-eco-groups</metric>
    <metric>dead-modern-groups</metric>
    <metric>dead-trad-groups</metric>
    <metric>dead-norm-groups</metric>
    <metric>dead-life-groups</metric>
    <metric>dead-ESG</metric>
    <metric>dead-ERG</metric>
    <metric>dead-PMG1</metric>
    <metric>dead-PMG2</metric>
    <metric>dead-PMGN</metric>
    <metric>dead-LMG</metric>
    <metric>dead-CMG</metric>
    <metric>mean [cond] of productive-groups with [culture = "ecologist"]</metric>
    <metric>mean [cond] of productive-groups with [culture = "modernist"]</metric>
    <metric>mean [cond] of productive-groups with [culture = "traditionalist"]</metric>
    <metric>mean [cond] of productive-groups with [culture = "normal"]</metric>
    <metric>agents-without-life-groups</metric>
    <metric>count productive-groups with [culture = "ecologist"]</metric>
    <metric>count productive-groups with [culture = "modernist"]</metric>
    <metric>count productive-groups with [culture = "traditionalist"]</metric>
    <metric>count productive-groups with [culture = "normal"]</metric>
    <metric>count persons with [culture = "ecologist"]</metric>
    <metric>count persons with [culture = "modernist"]</metric>
    <metric>count persons with [culture = "traditionalist"]</metric>
    <metric>count persons with [culture = "normal"]</metric>
    <metric>meta-inventory</metric>
    <metric>trad-inventory</metric>
    <metric>random-inventory</metric>
    <metric>eco-inventory</metric>
    <metric>modern-inventory</metric>
    <metric>total-donated-pm</metric>
    <metric>total-donated-pm-share</metric>
    <metric>total-donated-pm-by-norm</metric>
    <metric>total-donated-pm-by-trad</metric>
    <metric>total-donated-pm-by-eco</metric>
    <metric>total-donated-pm-by-mod</metric>
    <metric>total-recieved-pm-by-norm</metric>
    <metric>total-recieved-pm-by-trad</metric>
    <metric>total-recieved-pm-by-eco</metric>
    <metric>total-recieved-pm-by-mod</metric>
    <metric>count-new-support-links</metric>
    <metric>count-new-support-links-from-norm</metric>
    <metric>count-new-support-links-from-trad</metric>
    <metric>count-new-support-links-from-eco</metric>
    <metric>count-new-support-links-from-mod</metric>
    <metric>count-new-support-links-to-norm</metric>
    <metric>count-new-support-links-to-trad</metric>
    <metric>count-new-support-links-to-eco</metric>
    <metric>count-new-support-links-to-mod</metric>
    <metric>prod-network-variables</metric>
    <metric>mean [betweenness-centrality] of productive-groups with [culture = "ecologist"]</metric>
    <metric>mean [betweenness-centrality] of productive-groups with [culture = "modernist"]</metric>
    <metric>mean [betweenness-centrality] of productive-groups with [culture = "traditionalist"]</metric>
    <metric>mean [betweenness-centrality] of productive-groups with [culture = "normal"]</metric>
    <metric>mean [closeness-centrality] of productive-groups with [culture = "ecologist"]</metric>
    <metric>mean [closeness-centrality] of productive-groups with [culture = "modernist"]</metric>
    <metric>mean [closeness-centrality] of productive-groups with [culture = "traditionalist"]</metric>
    <metric>mean [closeness-centrality] of productive-groups with [culture = "normal"]</metric>
    <enumeratedValueSet variable="export-results">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="adoption-prob">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-recycled">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="culture-setup">
      <value value="&quot;equal&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-lg-time">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="care-behavior">
      <value value="&quot;normal&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LM-min">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tcm-min">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-ESG">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prod-list-size">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-LMG">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="please-draw-plots">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="iCM-min">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Experiment">
      <value value="&quot;base&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behaviorspace-experiments">
      <value value="&quot;exclusive-allocation-no-exp&quot;"/>
      <value value="&quot;exclusive-allocation-indirect-reciprocity&quot;"/>
      <value value="&quot;exclusive-allocation-bottom-up-signaling&quot;"/>
      <value value="&quot;exclusive-allocation-common-inventory&quot;"/>
      <value value="&quot;inclusive-allocation-no-exp&quot;"/>
      <value value="&quot;culture-dependent-allocation-no-exp&quot;"/>
      <value value="&quot;culture-dependent-allocation-indirect-reciprocity&quot;"/>
      <value value="&quot;culture-dependent-allocation-common-inventory&quot;"/>
      <value value="&quot;culture-dependent-allocation-bottom-up-signaling&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="icm-start">
      <value value="21"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="care-probability">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-CMG">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="care-time-connex">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-prod-list-size">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="group-culture">
      <value value="&quot;culture&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-ERG">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m-i">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="climate-experiment">
      <value value="&quot;baseline&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%damage_per_hit">
      <value value="9.4E-4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="status-consumption">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="c-i">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LM-start">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="env-impact">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-pmg-per-sector">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pm-donations">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%health_impact">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="work-at-other-culture">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="amplifier">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vitamin-b">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tcm-start">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="culture-prod-inclusiveness">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population-size">
      <value value="10000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Culture-adaptation">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="deliver-to-other-culture">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ego_param">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-PMG-sectors">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="culture-dominance-setup">
      <value value="0.6"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

arrow
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 150 150
Line -7500403 true 150 150 150 150
Polygon -7500403 true true 150 150 135 165 165 165 150 150
Polygon -7500403 true true 150 135 135 165 165 165 150 135
Polygon -7500403 true true 150 135 135 165 165 165 150 135
@#$#@#$#@
0
@#$#@#$#@
