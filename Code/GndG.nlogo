;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Copyright Society After Money ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; http://nach-dem-geld.de ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; The model is under development and frequently updated. ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;; This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License. ;;;;;;;;;;;;
;;;;;;;;;;;; To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/3.0/. ;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


__includes [ "draw_plots.nls" "update_agents.nls" "setup.nls" "go.nls"]

breed [persons person]

breed [life-groups life-group]
breed [productive-groups productive-group]
breed [coop-groups coop-group]

undirected-link-breed [memberships membership]
undirected-link-breed [coopships coopship]
directed-link-breed [prodlinks prodlink]

globals [

  ;;; Environment

  emission-stock
  emission-stock-old
  emission-multiplicator
  emission-stock-criticality

  sink
  sink-productivity

  ;;; Environmental conditions
  Env-R-cond   ; Condition of the environment (ERG-Current / ERG-target)
  Env-R-f-cond ; futur condition of the environment (R)

  Env-S-cond   ; Condition of the environment (ESG-Current / ESG-target)
  Env-S-f-cond ; futur condition of the environment (S)

  Env-cond ; Total condition of the environment (R & S)
  Env-f-cond ; Total futur condition of the environment (R & S)

  ;;; Society's condition
  soci-cond                  ; condition of the society
  soci-f-cond                  ; futur condition of the society

  ;;; Production Variables - set as globals for inital setup, then used to define productive groups
  labour-productivity-LMG
  labour-productivity-PMG
  labour-productivity-PMG1
  labour-productivity-PMG2...N-1
  labour-productivity-PMGN

  labour-productivity-ERG
  labour-productivity-ESG
  labour-productivity-CMG

  depriciation-LMG
  depriciation-PMG
  depriciation-ERG
  depriciation-ESG
  depriciation-CMG

  PM-intensity-LMG
  PM-intensity-PMG
  PM-intensity-ERG
  PM-intensity-ESG
  PM-intensity-CMG

  ER-intensity-PMG

  meta-inventory
  meta-waste-rate

  care-severity    ;severity of sickness a person in the care population needs --> z.b. normal distribution around min tcm   ; wie stark werden sie getroffen

]

persons-own [


  ;;; Characteristica / value

  ego-level ;-> how much do you value yourself compared to others / how much do you perceive others compared to yourself? (??????)
  leisure-level
  eco-level         ; same variable like groups, to compare for a wish to change
  prod-level        ; how important a high level of productivity is to the agent
  culture ; based on characterisica
          ;; Who of groups where agents are members or receive care goods.
  kind ; productive-group type variable
  my-prod
  my-LG
  my-CMG

  ;;; Needs
  ; sensual-vital
  care-impact
  SvN-iCM-current
  SvN-iCM-target
  SvN-LM-current
  SvN-LM-target
  SvN-tCM-current
  SvN-tCM-target
  ; productive
  sleep-time
  productive-time
  PrN-LG-current
  PrN-LG-target
  PrN-PrG-current
  PrN-PrG-target

  ;;; Conditions and evaluation of the conditions
  ;; Sensitive-vital need
  ; old
  SvN-cond-old
  SvN-iCM-cond-old
  SvN-LM-cond-old
  SvN-tCM-cond-old
  ; current
  SvN-cond
  SvN-iCM-cond
  SvN-LM-cond
  SvN-tCM-cond
  ; future
  SvN-f-cond
  SvN-iCM-f-cond
  SvN-LM-f-cond
  SvN-tCM-f-cond
  ; emotion & motivation
  SvN-emo
  SvN-moti
  SvN-iCM-emo
  SvN-iCM-moti
  SvN-tCM-moti
  SvN-LM-moti



  P-LG-SvN-iCM-cond-gm
  P-LG-SvN-tCM-cond-gm
  P-LG-SvN-LM-cond-gm

  SvN-tCM-cond

  SvN-iCM-f-cond
  SvN-tCM-f-cond
  SvN-LM-f-cond

  SvN-tCM-emo
  SvN-LM-emo



  ;; Productive-needs conditions
  PrN-cond-old
  PrN-LG-cond
  PrN-PrG-cond
  PrN-PrG-cond-history
  PrN-cond      ; condition of productive needs (Lage)
  PrN-f-cond    ; futur condition of productive needs
  PrN-emo       ; emotions regarding the productive needs
  PrN-moti      ; motivation regarding the productive needs

  ;; Personal conditions
  P-cond        ; total condition of person, combination aus SvN & PrN
  P-f-cond      ; futur condition of person
  P-emo         ; total emotions of person
  P-moti        ; total motivation of person
  P-cond-old

  ; Condumption history (good/bad)
  iCM-cond-b-history
  iCM-cond-g-history
  tCM-cond-b-history
  tCM-cond-g-history
  LM-cond-b-history
  LM-cond-g-history
  PrN-cond-b-history
  PrN-cond-g-history

  ;; condition of own groups
  ; old
  P-LG-cond-old
  p-PrG-cond-old
  p-coop-cond-old
  p-env-cond-old
  p-soci-cond-old
  ;current
  P-LG-cond
  p-PrG-cond
  p-coop-cond
  p-env-cond
  p-soci-cond
  ; future condition
  P-LG-f-cond
  p-PrG-f-cond
  p-coop-f-cond
  p-env-f-cond
  p-soci-f-cond
  ; emotional evaluation
  P-LG-cond-emo
  p-PrG-cond-emo
  p-coop-cond-emo
  p-env-cond-emo
  p-soci-cond-emo
  ; motivational evaluation
  P-LG-cond-moti
  p-PrG-cond-moti
  p-coop-cond-moti
  p-env-cond-moti
  p-soci-cond-moti
  ; groupmean
  P-LG-SvN-cond-gm
  P-LG-SvN-iCM-cond-gm
  P-LG-PrN-cond-gm
  P-PrG-SvN-cond-gm
  P-PrG-PrN-cond-gm

  ; Wellbeing
  P-wellbeing
  P-wellbeing-emo
  P-wellbeing-moti
  P-wellbeing-moti-old

  ; Priorities
  ;; priorities Sensual-vital Needs
  prio-SvN-iCM      ; priority for interpersonal CMG means
  prio-SvN-tCM      ; priority for transpersonal CMG means
  prio-SvN-LM       ; priority for consumption goods
                    ;; priorities Productive Needs
  prio-PrN-LG       ; priority for life-group
  prio-PrN-LMG       ; priority for prod-group
  prio-PrN-PMG      ; priority for PMG-group
  prio-PrN-CMG       ; priority for CMG-group
  prio-PrN-ESG
  prio-PrN-ERG
  prio-PrN-PrG      ; priority of the productive group the person is avtive in (e.g. active in PMG hence: prio-PrN-PrG = prio-PrN-PMG)
                    ;; priorities conditions
  prio-P-cond    ; priority for personal condition
  prio-LG-cond   ; priority for condition of own life-group
  prio-PrG-cond ; priority for condition of own productive group (group the person is active in)
  prio-coop-cond ; priority for condition of cooperative-group
  prio-env-cond  ; priority for condition of the environment
  prio-soci-cond ; priority for condition of the society

  ;;; Variables to facilitate procedures
  prod-satisfied? ;registeres a wish to change to a different productive group
  change-to  ; registers a wish to change in a different productive class, based on the priorities; only used if prod-satisfied? is false

]

life-groups-own [
  kind              ; life group
  cond              ; productive condition of life group (relation of iCM-current and iCM-target)
  cond-old
  time-cond-old
  time-cond                 ; remember previous time-condition
  prod-cond-old
  prod-cond
  f-cond            ; future condition of life group (WIE BERECHNET?)

  ; Production of ICMs
  iCM-labour-productivity   ; productivity of the persons producing iCM -> needed person-time for the production of one iCM
  iCM-ERG-intensity         ; resource-intensity-parameter -> needed eco for the production of one iCM
  iCM-emission-intensity    ; emission per produced iCM
  iCM-emission-intensity_abs
  iCM-pm-intensity         ; capital-intensity-parameter -> needed production means (mashines) for the production of one iCM
  iCM-current               ; interpersonal CMG means, current level [IST]
  iCM-target                ; interpersonal CMG means, target [SOLL]
                            ; Time used for production
  time-available           ; currently available person-time, in hours [Personenzeit]
  time-planned            ; needed person-time to achieve iCM-target
  time-cond-old
  time-cond              ; condition of time-availability (time-available / time-target)
  time-f-cond            ; future person-time needed (WIE BERECHNET?)

  ; Life-Group groupmeans
  SvN-iCM-gm
  SvN-tCM-gm
  SvN-LM-gm


  SvN-LM-needed
  SvN-LM-received
  SvN-LM-target
  SvN-LM-current
  SvN-LM-gap

  SvN-cond-gm
  SvN-moti-gm
  SvN-emo-gm

  SvN-iCM-emo-gm
  SvN-iCM-cond-gm
  SvN-iCM-moti-gm

  SvN-tCM-cond-gm
  SvN-LM-cond-gm
  SvN-tCM-emo-gm
  SvN-LM-emo-gm

  SvN-tCM-moti-gm
  SvN-LM-moti-gm

  PrN-emo-gm
  PrN-moti-gm
  PrN-cond-gm


  P-emo-gm
  P-LG-cond-emo-gm
  p-PrG-cond-emo-gm
  p-env-cond-emo-gm
  p-soci-cond-emo-gm

  P-moti-gm
  P-LG-cond-moti-gm
  p-PrG-cond-moti-gm
  p-env-cond-moti-gm
  p-soci-cond-moti-gm

  p-cond-gm
  p-f-cond-gm

  ; Wellbeing
  LG-wellbeing              ; mean wellbeing (satisfaction) of all members of this groups

  total-gaps

]

productive-groups-own [
  kind             ; ===== membership-type of link between person and group  --> PMG LMG CMG AG ERG ESG
  Sector-level
  culture
  prod-inclusiveness
  update-group-culture-counter


  ; Production
  ;; Labour relations
  labour-productivity   ; productivity of the persons producing PM -> needed person-time for the production of one PM
  time-needed
  time-available         ; currently available person-time, in hours (Personenzeit)
  time-used
  time-planned
  ;; Machinery
  depriciation
  pm-intensity         ; capital-intensity-parameter -> needed production means (mashines) for the production of one PM
  available-machines    ; cap-intensity *PM-current
  available-resources   ; ER-intesntiy * ER-current
  PM-current           ; production means currently available in units [Stück]
  PM-target            ; target: production means needed in units [Stück]
  PM-needed
  PM-received
  ;; Relations to the environment
  ;;; Plans
  planned-emission
  planned-sink-distruction
  ;;; Resource
  ER-current           ; current eco-R-means-volume in units [Stück]
  ER-target            ; target: eco-R-means needed in units [Stück]
  ER-needed
  ER-received
  ER-intensity         ; resource-intensity-parameter -> needed eco for the production of one PM
                       ;;; Emissions
  emission
  emission-intensity
  emission-intensity_abs
  ;;; Sinks
  sink-intensity ; how many sinks are destroyed during the production process
  sink-current         ; current eco-S-means-volume in units [Stück] --> ev. entfernen wenn nicht nötig.
  sink-target          ; target: eco-S-means needed in units [Stück]
                       ;; Relations to agents

  ; Supply, Demand, Inventory
  ;; Demand
  total-demand
  total-provision
  demand-history
  ;; Output
  planned-output
  output
  output-history
  prod-adjustment      ; adjustmet to the planned production volume
                       ;; Inventory
  inventory            ; current production-volume of PM (production means) in units (Stück) [IST]
  reserve-target       ; inventory left over after consumption
  reserve-target-%     ; % of inventory groups want to have after consumption, alpha1 in extractivist model (production reserve stock rate)
  waste-rate           ; % of inventory that is destroyed after each week

  ; Group conditions
  cond-old
  cond
  time-cond-old
  time-cond
  PM-cond-old
  PM-cond
  prod-cond-old
  prod-cond
  planning-cond-old
  planning-cond
  demand-cond-old
  demand-cond
  ER-cond-old
  ER-cond
  eco-cond-old
  eco-cond


  ; Group mean of group-members conditions
  ;; SVN Conditions, Emotions, and Motivations
  SvN-cond-gm
  SvN-LM-gm
  SvN-tCM-gm
  SvN-iCM-gm

  SvN-emo-gm
  SvN-iCM-emo-gm
  SvN-moti-gm
  SvN-iCM-moti-gm
  SvN-tCM-cond-gm
  SvN-LM-cond-gm
  SvN-tCM-emo-gm
  SvN-LM-emo-gm

  SvN-iCM-cond-gm

  SvN-tCM-moti-gm
  SvN-LM-moti-gm
  ;; PRN Condition
  PrN-cond-gm

  PrN-emo-gm
  PrN-moti-gm
  ;; Personal conditions
  P-emo-gm
  P-LG-cond-emo-gm
  p-PrG-cond-emo-gm
  p-env-cond-emo-gm
  p-soci-cond-emo-gm
  P-moti-gm
  P-LG-cond-moti-gm
  p-PrG-cond-moti-gm
  p-env-cond-moti-gm
  p-soci-cond-moti-gm

  ego-level-gm
  eco-level-gm
  prod-level-gm
  leisure-level-gm

  ; Wellbeing
  LMG-wellbeing            ; mean wellbeing (satisfaction of all members of this groups (Mean p-wellbeing)
]


memberships-own [
  membership-type
]

prodlinks-own [
  prodlink-type
  out-weight ; weight of a link from the perspective of the sending agent
  in-weight  ; weight of a link from the perspective of the receiving agent
  out-kind
  in-kind
  demand
  provision

]


to go
  go-update
end
@#$#@#$#@
GRAPHICS-WINDOW
162
417
412
616
-1
-1
5.762
1
10
1
1
1
0
0
0
1
-1
40
-30
2
0
0
1
ticks
30.0

BUTTON
13
37
76
70
setup
setup
NIL
1
T
OBSERVER
NIL
S
NIL
NIL
1

SLIDER
0
262
122
295
num-ERG
num-ERG
0
10
3.0
1
1
NIL
HORIZONTAL

SLIDER
0
340
125
373
num-PMG
num-PMG
0
30
12.0
1
1
NIL
HORIZONTAL

SLIDER
3
416
126
449
num-LMG
num-LMG
0
50
4.0
1
1
NIL
HORIZONTAL

SLIDER
3
455
128
488
num-CMG
num-CMG
0
50
4.0
1
1
NIL
HORIZONTAL

SLIDER
12
169
134
202
population-size
population-size
0
50000
5000.0
100
1
NIL
HORIZONTAL

PLOT
925
37
1085
227
life-groups-histo
members per life-group
number of life-groups
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

BUTTON
79
38
142
71
go
go
T
1
T
OBSERVER
NIL
G
NIL
NIL
1

TEXTBOX
1
241
151
259
Number of Groups
14
0.0
1

PLOT
608
35
919
228
groups-histo
members per group
number of groups
0.0
10.0
0.0
15.0
false
true
"" ""
PENS

MONITOR
524
36
599
81
LMG active
sum [count my-memberships] of productive-groups with [kind = \"LMG\"]
17
1
11

MONITOR
524
86
600
131
PMG active
sum [count my-memberships] of productive-groups with [kind = \"PMG\"]
17
1
11

MONITOR
525
133
600
178
CMG active
sum [count my-memberships] of productive-groups with [kind = \"CMG\"]
17
1
11

MONITOR
526
181
600
226
ERG active
sum [count my-memberships] of productive-groups with [kind = \"ERG\"]
17
1
11

TEXTBOX
525
12
696
46
Agents active in sectors
14
0.0
1

MONITOR
526
230
600
275
ESG active
sum [count my-memberships] of productive-groups with [kind = \"ESG\"]
17
1
11

SLIDER
0
302
123
335
num-ESG
num-ESG
0
10
3.0
1
1
NIL
HORIZONTAL

PLOT
2335
356
2535
506
Distribution-Leasure-level
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2127
354
2327
504
Distribution-Ego-level
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1742
34
1974
214
Distribution-active-hours
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SLIDER
1
375
126
408
num-PMG-sectors
num-PMG-sectors
0
10
3.0
1
1
NIL
HORIZONTAL

SLIDER
6
505
132
538
prod-list-size
prod-list-size
0
10
3.0
1
1
NIL
HORIZONTAL

MONITOR
1075
1158
1178
1203
LMG output (avg)
precision (mean [output] of productive-groups with [kind = \"LMG\"]) 3
17
1
11

MONITOR
1190
1157
1241
1202
LMG output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"LMG\"]) 3
17
1
11

MONITOR
994
1156
1066
1201
PMG 3 output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"PMG\" and sector-level = 3]) 3
17
1
11

MONITOR
867
1152
989
1197
PMG 3 output (avg)
precision (mean [output] of productive-groups with [kind = \"PMG\" and sector-level = 3]) 3
17
1
11

MONITOR
763
1153
845
1198
PMG 2 output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"PMG\" and sector-level = 2]) 3
17
1
11

MONITOR
653
1152
758
1197
PMG 2 output (avg)
precision (mean [output] of productive-groups with [kind = \"PMG\" and sector-level = 2]) 3
17
1
11

MONITOR
563
1150
614
1195
PMG 1 output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"PMG\" and sector-level = 1]) 3
17
1
11

MONITOR
446
1149
558
1194
PMG1 output (avg)
precision (mean [output] of productive-groups with [kind = \"PMG\" and sector-level = 1]) 3
17
1
11

MONITOR
351
1155
402
1200
ERG output (stvd)
precision (standard-deviation [output] of productive-groups with [kind = \"ERG\"]) 3
17
1
11

MONITOR
241
1154
345
1199
ERG output (avg)
precision (mean [output] of productive-groups with [kind = \"ERG\"]) 3
17
1
11

MONITOR
1020
916
1115
961
SvN-LM-target
precision (sum [SvN-LM-target] of persons) 3
17
1
11

MONITOR
1119
917
1215
962
LM Scarcity
precision (sum [output] of productive-groups with [kind = \"LMG\"] - sum [SvN-LM-target] of persons) 3
17
1
11

PLOT
20
974
350
1148
Production (Avg per group)
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
1299
34
1531
215
Time
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
443
491
742
630
SvN-Condition
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
442
636
741
783
PrN-Condition
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1010
492
1262
636
Meso/Macro: conditions
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1590
681
1848
830
SvN Priorities
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1589
545
1889
677
PrN Priorities
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
751
492
1003
631
SvN Emo & Moti
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1009
639
1264
809
Meso/Macro: Emo & Moti
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

MONITOR
1092
37
1282
82
population not in productive groups
count persons with [not any? membership-neighbors with [breed = productive-groups]]
17
1
11

PLOT
1095
87
1281
227
population not in productive groups
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
234
1205
434
1355
ERG: Supply, Demand, Output, Inventory
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
441
1204
641
1354
PMG1: Supply, Demand, Output, Inventory, Consumption
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
650
1207
856
1353
PMG2: Supply, Demand, Output, Inventory, Consumption
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
864
1208
1065
1352
PMG3: Supply, Demand, Output, Inventory, Consumption
NIL
NIL
0.0
10.0
-1.0
1.0
true
false
"" ""
PENS

PLOT
1015
968
1394
1143
LMG: Supply, Demand, Output, Inventory, Consumption
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
1075
1361
1281
1511
LMG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
235
1361
435
1511
ERG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
439
1364
643
1504
PMG1: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
651
1361
858
1512
PMG2: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
864
1362
1065
1510
PMG3: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
235
1518
436
1667
ERG: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
235
1674
436
1823
ERG: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
446
1518
647
1665
PMG1: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
655
1519
856
1666
PMG2: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
865
1519
1066
1666
PMG3: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1075
1519
1281
1665
LMG: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
446
1674
647
1824
PMG1: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
657
1676
858
1823
PMG2: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
866
1675
1067
1823
PMG3: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1076
1676
1282
1824
LMG: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
669
972
1009
1134
Inventories
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
25
1205
226
1355
ESG: Sinks required and produced
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

MONITOR
1306
1157
1410
1202
CMG output (avg)
mean [output] of productive-groups with [kind = \"CMG\"]
17
1
11

MONITOR
24
1155
125
1200
ESG output (avg)
mean [output] of productive-groups with [kind = \"ESG\"]
17
1
11

PLOT
751
639
1003
779
PrN Emo & Moti
NIL
NIL
0.0
1.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1069
1205
1284
1353
LMG: Supply, Demand, Output, Inventory, Consumption1
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
25
1361
226
1511
ESG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
25
1673
226
1823
ESG: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
24
1517
225
1667
ESG: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
22
806
354
929
Pollution and sinks (biocapacity)
NIL
NIL
0.0
10.0
-10000.0
10.0
true
true
"" ""
PENS

PLOT
1290
1208
1491
1352
CMG: Supply, Demand, Output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1291
1520
1492
1665
CMG: PM-target vs current
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1292
1361
1494
1510
CMG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
1292
1677
1487
1825
CMG: prod-target vs output
NIL
NIL
0.0
10.0
-1.0
10.0
true
false
"" ""
PENS

PLOT
25
1830
226
1980
ESG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
235
1830
435
1980
ERG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
446
1832
646
1979
PMG1-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
657
1831
858
1978
PMG2-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
866
1832
1067
1978
PMG3-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
1074
1832
1282
1978
LMG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
1292
1833
1492
1977
CMG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
1428
966
1707
1185
Group Conditions
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
1498
1211
1699
1351
LG: Supply, Demand, Output
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

PLOT
1501
1833
1701
1978
LG-cond
NIL
NIL
0.0
10.0
-0.1
1.1
true
false
"" ""
PENS

PLOT
1501
1359
1699
1508
LG: Production Limit
NIL
NIL
0.0
10.0
-1.0
10.0
true
true
"" ""
PENS

SWITCH
12
206
134
239
export-results
export-results
1
1
-1000

PLOT
1746
964
2133
1210
Productive-time by sector
NIL
NIL
0.0
10.0
-1000.0
10.0
true
true
"" ""
PENS

PLOT
235
1989
435
2136
ERG:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
445
1988
645
2138
PMG1:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
657
1987
857
2135
PMG2:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
866
1988
1068
2136
PMG3:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1076
1987
1280
2137
LMG:Gap_supply-demand
NIL
NIL
0.0
10.0
-10.0
10.0
true
false
"" ""
PENS

PLOT
1293
1987
1493
2137
CMG:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
25
1987
227
2137
ESG:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1503
1987
1703
2137
LG:Gap_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
342
2141
543
2291
PMG1:Gap_ER_supply-demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
548
2140
749
2290
PMG1:resources-received
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
357
974
645
1140
Time-to-machine limit
NIL
NIL
0.0
10.0
-1.0
1.0
true
true
"" ""
PENS

PLOT
442
327
751
480
Wellbeing
NIL
NIL
0.0
1.0
0.0
1.0
true
true
"" ""
PENS

MONITOR
2567
294
2674
339
Ecologists (culture)
count persons with [culture = \"ecologist\"]
17
1
11

MONITOR
2569
488
2698
533
Modernists (culture)
count persons with [culture = \"modernist\"]
17
1
11

MONITOR
2567
391
2699
436
Traditonalists (cutlure)
count persons with [culture = \"traditionalist\"]
17
1
11

PLOT
2125
57
2349
185
group-cultures
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

CHOOSER
165
245
303
290
group-culture
group-culture
"culture" "normal"
1

MONITOR
2566
197
2677
242
Normals (Culture)
count persons with [culture = \"normal\"]
17
1
11

PLOT
1747
1215
2132
1453
reserve-target by group
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
2352
58
2584
185
Group Conditions by Culture
NIL
NIL
0.0
10.0
-0.1
1.1
true
true
"" ""
PENS

PLOT
2127
200
2327
350
Distribution-Prod-level
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2335
200
2535
350
Distribution-Eco-level
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SWITCH
167
296
328
329
Culture-adaptation
Culture-adaptation
0
1
-1000

PLOT
2127
510
2327
660
Ecologists-characters
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
2334
511
2534
661
Modernists-characters
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2129
666
2329
816
Traditionalists-characters
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2335
666
2535
817
normals-characters
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SLIDER
167
335
265
368
adoption-prob
adoption-prob
0
1
1.0
0.1
1
NIL
HORIZONTAL

MONITOR
2567
342
2674
387
avg groupsize ecologists
mean[count membership-neighbors] of productive-groups with [culture = \"ecologist\"]
17
1
11

MONITOR
2567
244
2677
289
avg groupsize normals
mean[count membership-neighbors] of productive-groups with [culture = \"normal\"]
17
1
11

MONITOR
2569
535
2701
580
avg groupsize modernists
mean[count membership-neighbors] of productive-groups with [culture = \"modernist\"]
17
1
11

MONITOR
2567
437
2704
482
avg groupsize traditionalists
mean[count membership-neighbors] of productive-groups with [culture = \"traditionalist\"]
17
1
11

PLOT
748
785
998
905
SvN-Cond-futur
NIL
NIL
0.0
10.0
0.0
1.0
true
true
"" ""
PENS

SLIDER
315
76
407
109
icm-start
icm-start
0
100
21.0
1
1
NIL
HORIZONTAL

SLIDER
411
76
503
109
LM-start
LM-start
0
100
5.0
1
1
NIL
HORIZONTAL

SLIDER
315
146
407
179
tcm-start
tcm-start
0
100
5.0
1
1
NIL
HORIZONTAL

SLIDER
410
111
502
144
LM-min
LM-min
0
100
3.0
1
1
NIL
HORIZONTAL

SLIDER
315
111
407
144
iCM-min
iCM-min
0
100
20.0
1
1
NIL
HORIZONTAL

SLIDER
411
146
503
179
tcm-min
tcm-min
0
100
4.0
1
1
NIL
HORIZONTAL

TEXTBOX
2296
1821
3030
2025
16.12.:\n- decline of all outputs if PMG1 has cap depriciation. More stable if depriciation is 0 (NOW SET TO 0)\n\nTO DO\n- Priorities of prod groups (used for entry/exit): ERG has a lack of workers --> change of priorities has to somehow be linked to the situation in all sectors --> how do agents know, that people are needed at the very beginning of the production chain, and how are they motivated to join this goup --> Really check changes in priorities and how these influence decisions, also regarding work time.\n- Change of group connections (procurement process/ vendor-list, based on preferences (?); \"Stammkunden\"; Liefererpräferenz; Verteilungsmechanismus\n- CMG reparieren (evt.mit entry/exit?)\n- Indikator für Gruppengrößen\n
14
0.0
1

PLOT
1590
330
1980
538
PrN priorities per sector
NIL
NIL
0.0
1.0
0.0
0.6
true
true
"" ""
PENS

PLOT
2140
965
2551
1211
Agents per sector
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
1744
1477
1975
1634
Meta-Inventory
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
2143
1217
2552
1453
Prn-prg-target per agents per sector
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1746
1685
1973
1805
PMG1: demand vs planned-output
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

PLOT
1377
332
1544
482
care-impact
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS

PLOT
758
328
958
478
tcm demand
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

PLOT
965
329
1165
479
icm demand
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS

MONITOR
1171
283
1282
328
People needing care
count persons with [care-impact < 1]
17
1
11

PLOT
1170
330
1370
480
people needing care
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SLIDER
166
77
291
110
care-probability
care-probability
0
0.3
0.07
0.01
1
NIL
HORIZONTAL

PLOT
1270
494
1531
635
tcm & icm emo
NIL
NIL
0.0
10.0
0.0
1.0
true
true
"" ""
PENS

SLIDER
165
116
293
149
care-time-connex
care-time-connex
0
1
0.5
0.1
1
NIL
HORIZONTAL

PLOT
1541
35
1736
215
time-target
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

SLIDER
165
153
294
186
min-lg-time
min-lg-time
0
40
6.0
1
1
NIL
HORIZONTAL

SWITCH
165
197
255
230
m-i
m-i
0
1
-1000

SLIDER
13
129
133
162
ego_param
ego_param
0
1
0.5
0.1
1
NIL
HORIZONTAL

PLOT
360
807
558
927
Global Pollution
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS

CHOOSER
13
77
133
122
Experiment
Experiment
"base" "care" "climate"
0

TEXTBOX
19
10
169
28
Setup
14
0.0
1

TEXTBOX
31
952
181
970
Production
14
0.0
1

TEXTBOX
27
778
177
796
Biophysical
14
0.0
1

TEXTBOX
10
537
144
579
should not be bigger than smallest number of groups per sector
11
0.0
1

TEXTBOX
167
393
317
411
Production network
11
0.0
1

TEXTBOX
2126
33
2276
51
Culture-related Plots
11
0.0
1

TEXTBOX
1302
10
1452
28
Time
14
0.0
1

TEXTBOX
444
302
594
320
Agents
14
0.0
1

TEXTBOX
1757
930
1907
948
Overview production
14
0.0
1

TEXTBOX
313
10
499
38
Deselect \"view updates\" (see above) for faster runs
11
0.0
1

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

building store
false
0
Rectangle -7500403 true true 30 45 45 240
Rectangle -16777216 false false 30 45 45 165
Rectangle -7500403 true true 15 165 285 255
Rectangle -16777216 true false 120 195 180 255
Line -7500403 true 150 195 150 255
Rectangle -16777216 true false 30 180 105 240
Rectangle -16777216 true false 195 180 270 240
Line -16777216 false 0 165 300 165
Polygon -7500403 true true 0 165 45 135 60 90 240 90 255 135 300 165
Rectangle -7500403 true true 0 0 75 45
Rectangle -16777216 false false 0 0 75 45

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

factory
false
0
Rectangle -7500403 true true 76 194 285 270
Rectangle -7500403 true true 36 95 59 231
Rectangle -16777216 true false 90 210 270 240
Line -7500403 true 90 195 90 255
Line -7500403 true 120 195 120 255
Line -7500403 true 150 195 150 240
Line -7500403 true 180 195 180 255
Line -7500403 true 210 210 210 240
Line -7500403 true 240 210 240 240
Line -7500403 true 90 225 270 225
Circle -1 true false 37 73 32
Circle -1 true false 55 38 54
Circle -1 true false 96 21 42
Circle -1 true false 105 40 32
Circle -1 true false 129 19 42
Rectangle -7500403 true true 14 228 78 270

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

orbit 1
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 false true 41 41 218

orbit 2
true
0
Circle -7500403 true true 116 221 67
Circle -7500403 true true 116 11 67
Circle -7500403 false true 44 44 212

orbit 3
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 true true 26 176 67
Circle -7500403 true true 206 176 67
Circle -7500403 false true 45 45 210

orbit 4
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 true true 116 221 67
Circle -7500403 true true 221 116 67
Circle -7500403 false true 45 45 210
Circle -7500403 true true 11 116 67

orbit 5
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 true true 13 89 67
Circle -7500403 true true 178 206 67
Circle -7500403 true true 53 204 67
Circle -7500403 true true 220 91 67
Circle -7500403 false true 45 45 210

orbit 6
true
0
Circle -7500403 true true 116 11 67
Circle -7500403 true true 26 176 67
Circle -7500403 true true 206 176 67
Circle -7500403 false true 45 45 210
Circle -7500403 true true 26 58 67
Circle -7500403 true true 206 58 67
Circle -7500403 true true 116 221 67

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="comsim_18022022" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3000"/>
    <metric>mean [P-wellbeing-moti] of persons</metric>
    <metric>min [P-wellbeing-moti] of persons</metric>
    <metric>mean [P-wellbeing-emo] of persons</metric>
    <metric>min [P-wellbeing-emo] of persons</metric>
    <metric>sum [iCM-current] of life-groups</metric>
    <metric>sum [SvN-LM-current] of persons</metric>
    <metric>sum [SvN-icm-current] of persons</metric>
    <metric>sum [SvN-tcm-current] of persons</metric>
    <metric>sum [SvN-LM-target] of persons</metric>
    <metric>sum [SvN-icm-target] of persons</metric>
    <metric>sum [SvN-tcm-target] of persons</metric>
    <metric>mean [prio-PrN-LG] of persons</metric>
    <metric>mean [prio-PrN-PMG] of persons</metric>
    <metric>mean [prio-PrN-CMG] of persons</metric>
    <metric>mean [prio-PrN-ERG] of persons</metric>
    <metric>mean [prio-PrN-ESG] of persons</metric>
    <metric>mean [prio-PrN-LMG] of persons</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups]] / count persons</metric>
    <metric>mean [SvN-Cond] of persons</metric>
    <metric>mean [SvN-iCM-Cond] of persons</metric>
    <metric>mean [SvN-LM-Cond] of persons</metric>
    <metric>mean [SvN-tCM-Cond] of persons</metric>
    <metric>min [SvN-Cond] of persons</metric>
    <metric>min [SvN-iCM-Cond] of persons</metric>
    <metric>min [SvN-LM-Cond] of persons</metric>
    <metric>min [SvN-tCM-Cond] of persons</metric>
    <metric>mean [PrN-LG-current] of persons</metric>
    <metric>mean [PrN-LG-target] of persons</metric>
    <metric>mean [PrN-Cond] of persons</metric>
    <metric>count persons with [care-impact &lt; 1]</metric>
    <metric>mean [care-impact] of persons</metric>
    <metric>median [care-impact] of persons</metric>
    <metric>mean [cond] of productive-groups with [kind = "ERG"]</metric>
    <metric>mean [cond] of productive-groups with [kind = "ESG"]</metric>
    <metric>mean [cond] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>mean [cond] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>mean [cond] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>mean [cond] of productive-groups with [kind = "CMG"]</metric>
    <metric>mean [cond] of productive-groups with [kind = "LMG"]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "PMG"]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "LMG"]</metric>
    <metric>meta-inventory</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "PMG"]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "LMG"]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "ESG"]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "CMG"]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "LMG"]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "ESG"]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "CMG"]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "LMG"]</metric>
    <metric>mean [cond] of productive-groups with [culture = "normal"]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "LMG"]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "CMG"]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "ERG"]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "ESG"]</metric>
    <metric>emission-stock</metric>
    <metric>sink * sink-productivity</metric>
    <metric>sum [emission] of productive-groups</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "ESG"] * sink-productivity</metric>
    <metric>sum [output] of productive-groups with [kind = "ESG"] * sink-productivity</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [SvN-LM-target] of persons</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "CMG"]</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "ESG"]</metric>
    <metric>sum [iCM-target] of life-groups</metric>
    <metric>sum [output] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [output] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [output] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [output] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [output] of productive-groups with [kind = "LMG"]</metric>
    <metric>sum [output] of productive-groups with [kind = "CMG"]</metric>
    <metric>sum [output] of productive-groups with [kind = "ESG"]</metric>
    <metric>ticks</metric>
    <enumeratedValueSet variable="adoption-prob">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-lg-time">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-ESG-initial">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LM-min">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tcm-min">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-ESG">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prod-list-size">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-LMG">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="iCM-min">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Experiment">
      <value value="&quot;base&quot;"/>
      <value value="&quot;care&quot;"/>
      <value value="&quot;climate&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-CMG-initial">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="icm-start">
      <value value="21"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-ERG-initial">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="care-probability">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-CMG">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="group-culture">
      <value value="&quot;normal&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-AG-initial">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="care-time-connex">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-ERG">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-LMG-initial">
      <value value="0.31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m-i">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-PMG-initial">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LM-start">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tcm-start">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-PMG">
      <value value="24"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population-size">
      <value value="30000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Culture-adaptation">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="export-results">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-PMG-sectors">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ego_param">
      <value value="0.25"/>
      <value value="0.5"/>
      <value value="0.75"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="comsim_18022022_placeholder" repetitions="2" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="500"/>
    <metric>mean [P-wellbeing-moti] of persons</metric>
    <metric>min [P-wellbeing-moti] of persons</metric>
    <metric>mean [P-wellbeing-emo] of persons</metric>
    <metric>min [P-wellbeing-emo] of persons</metric>
    <metric>sum [iCM-current] of life-groups</metric>
    <metric>sum [SvN-LM-current] of persons</metric>
    <metric>sum [SvN-icm-current] of persons</metric>
    <metric>sum [SvN-tcm-current] of persons</metric>
    <metric>sum [SvN-LM-target] of persons</metric>
    <metric>sum [SvN-icm-target] of persons</metric>
    <metric>sum [SvN-tcm-target] of persons</metric>
    <metric>mean [prio-PrN-LG] of persons</metric>
    <metric>mean [prio-PrN-PMG] of persons</metric>
    <metric>mean [prio-PrN-CMG] of persons</metric>
    <metric>mean [prio-PrN-ERG] of persons</metric>
    <metric>mean [prio-PrN-ESG] of persons</metric>
    <metric>mean [prio-PrN-LMG] of persons</metric>
    <metric>count persons with [not any? membership-neighbors with [breed = productive-groups]] / count persons</metric>
    <metric>mean [SvN-Cond] of persons</metric>
    <metric>mean [SvN-iCM-Cond] of persons</metric>
    <metric>mean [SvN-LM-Cond] of persons</metric>
    <metric>mean [SvN-tCM-Cond] of persons</metric>
    <metric>min [SvN-Cond] of persons</metric>
    <metric>min [SvN-iCM-Cond] of persons</metric>
    <metric>min [SvN-LM-Cond] of persons</metric>
    <metric>min [SvN-tCM-Cond] of persons</metric>
    <metric>mean [PrN-LG-current] of persons</metric>
    <metric>mean [PrN-LG-target] of persons</metric>
    <metric>mean [PrN-Cond] of persons</metric>
    <metric>count persons with [care-impact &lt; 1]</metric>
    <metric>mean [care-impact] of persons</metric>
    <metric>median [care-impact] of persons</metric>
    <metric>mean [cond] of productive-groups with [kind = "ERG"]</metric>
    <metric>mean [cond] of productive-groups with [kind = "ESG"]</metric>
    <metric>mean [cond] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>mean [cond] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>mean [cond] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>mean [cond] of productive-groups with [kind = "CMG"]</metric>
    <metric>mean [cond] of productive-groups with [kind = "LMG"]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "PMG"]</metric>
    <metric>sum [inventory] of productive-groups with [kind = "LMG"]</metric>
    <metric>meta-inventory</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "PMG"]</metric>
    <metric>sum [reserve-target] of productive-groups with [kind = "LMG"]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "ESG"]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "CMG"]</metric>
    <metric>sum [count membership-neighbors] of productive-groups with [kind = "LMG"]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "ESG"]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "CMG"]</metric>
    <metric>sum [time-used] of productive-groups with [kind = "LMG"]</metric>
    <metric>mean [cond] of productive-groups with [culture = "normal"]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "LMG"]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "CMG"]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "ERG"]</metric>
    <metric>mean [count membership-neighbors with [breed = persons]] of productive-groups with [kind = "ESG"]</metric>
    <metric>emission-stock</metric>
    <metric>sink * sink-productivity</metric>
    <metric>sum [emission] of productive-groups</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "ESG"] * sink-productivity</metric>
    <metric>sum [output] of productive-groups with [kind = "ESG"] * sink-productivity</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [SvN-LM-target] of persons</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "CMG"]</metric>
    <metric>sum [total-demand] of productive-groups with [kind = "ESG"]</metric>
    <metric>sum [iCM-target] of life-groups</metric>
    <metric>sum [output] of productive-groups with [kind = "ERG"]</metric>
    <metric>sum [output] of productive-groups with [kind = "PMG" and sector-level = 1]</metric>
    <metric>sum [output] of productive-groups with [kind = "PMG" and sector-level = 2]</metric>
    <metric>sum [output] of productive-groups with [kind = "PMG" and sector-level = 3]</metric>
    <metric>sum [output] of productive-groups with [kind = "LMG"]</metric>
    <metric>sum [output] of productive-groups with [kind = "CMG"]</metric>
    <metric>sum [output] of productive-groups with [kind = "ESG"]</metric>
    <metric>ticks</metric>
    <enumeratedValueSet variable="adoption-prob">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-lg-time">
      <value value="6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-ESG-initial">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LM-min">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tcm-min">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-ESG">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="prod-list-size">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-LMG">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="iCM-min">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Experiment">
      <value value="&quot;base&quot;"/>
      <value value="&quot;care&quot;"/>
      <value value="&quot;climate&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-CMG-initial">
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="icm-start">
      <value value="21"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-ERG-initial">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="care-probability">
      <value value="0.07"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-CMG">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="group-culture">
      <value value="&quot;normal&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-AG-initial">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="care-time-connex">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-ERG">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-LMG-initial">
      <value value="0.31"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m-i">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="%-PMG-initial">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="LM-start">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="tcm-start">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-PMG">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population-size">
      <value value="1000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Culture-adaptation">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="export-results">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-PMG-sectors">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ego_param">
      <value value="0.25"/>
      <value value="0.5"/>
      <value value="0.75"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

arrow
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 150 150
Line -7500403 true 150 150 150 150
Polygon -7500403 true true 150 150 135 165 165 165 150 150
Polygon -7500403 true true 150 135 135 165 165 165 150 135
Polygon -7500403 true true 150 135 135 165 165 165 150 135
@#$#@#$#@
0
@#$#@#$#@
